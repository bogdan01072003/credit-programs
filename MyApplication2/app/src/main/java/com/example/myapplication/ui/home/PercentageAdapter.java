package com.example.myapplication.ui.home;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import androidx.annotation.NonNull;

import java.util.List;

public class PercentageAdapter extends ArrayAdapter<Double> {

  public PercentageAdapter(Context context, List<Double> percentages) {
    super(context, android.R.layout.simple_spinner_item, percentages);
    setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
  }

  @NonNull
  @Override
  public View getView(int position, View convertView, @NonNull ViewGroup parent) {
    TextView view = (TextView) super.getView(position, convertView, parent);
    view.setText(String.format("%s%%", getItem(position)));
    return view;
  }

  @Override
  public View getDropDownView(int position, View convertView, @NonNull ViewGroup parent) {
    TextView view = (TextView) super.getDropDownView(position, convertView, parent);
    view.setText(String.format("%s%%", getItem(position)));
    return view;
  }
}
