package com.example.myapplication.ui.home;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.widget.SeekBar;
import androidx.annotation.NonNull;
import androidx.appcompat.widget.PopupMenu;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import com.example.myapplication.R;
import com.example.myapplication.databinding.FragmentHomeBinding;
import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import java.util.ArrayList;


public class HomeFragment extends Fragment {

    private FragmentHomeBinding binding;
    private HomeViewModel homeViewModel;
    private LineChart lineChart;
    private long lastSavedId = -1;
    private DatabaseHelper databaseHelper;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        databaseHelper = new DatabaseHelper(requireContext());
    }

    @Override
    public void onResume() {
        super.onResume();
        databaseHelper.getReadableDatabase();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        binding = FragmentHomeBinding.inflate(inflater, container, false);
        View root = binding.getRoot();

        homeViewModel = new ViewModelProvider(this).get(HomeViewModel.class);

        lineChart = root.findViewById(R.id.lineChart);
        homeViewModel.getLoanAmount().observe(getViewLifecycleOwner(), loanAmount -> {
            if (loanAmount != null) {
                binding.editTextLoanAmount.setText(String.valueOf(loanAmount));
            }
        });

        homeViewModel.getRepaymentAmount().observe(getViewLifecycleOwner(), repaymentAmount -> {
            if (repaymentAmount != null) {
                binding.editTextRepaymentAmount.setText(String.valueOf(repaymentAmount));
            }
        });

        homeViewModel.getPercent().observe(getViewLifecycleOwner(), percent -> {
            if (percent != null) {
                binding.editTextPercentAmount.setText(String.valueOf(percent));
            }
        });

        binding.button.setOnClickListener(view -> calculateDebt());
        binding.button.setOnClickListener(view -> setupLineChart());
        binding.buttonSave.setOnClickListener(view -> saveDataToDatabase());
        binding.buttonOpen.setOnClickListener(view -> showPopupMenu(view));



        binding.seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                calculateDebt();
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

        return root;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        binding = null;
    }
    private void setupLineChart() {
       double loanAmount = Double.parseDouble(binding.editTextLoanAmount.getText().toString());
        double repaymentAmount = Double.parseDouble(binding.editTextRepaymentAmount.getText().toString());
        double percent = Double.parseDouble(binding.editTextPercentAmount.getText().toString());
        ArrayList<Entry> entries = new ArrayList<>();
        percent/=100;
        int numberOfPayments = (int) Math.ceil(loanAmount * (1 + percent) / repaymentAmount);
        for (int i = 0; i < numberOfPayments; i++) {
            double debt = (loanAmount * (1+ percent) - i * repaymentAmount);
            entries.add(new Entry(i, (float) debt));
        }

        LineDataSet dataSet = new LineDataSet(entries, "Debt vs NumberOfPayments");
        LineData lineData = new LineData(dataSet);
        lineChart.setBackgroundColor(Color.WHITE);

        lineChart.setData(lineData);
        lineChart.getDescription().setEnabled(false);
        lineChart.invalidate();
    }
    private void calculateDebt() {
        double loanAmount = Double.parseDouble(binding.editTextLoanAmount.getText().toString());
        double repaymentAmount = Double.parseDouble(binding.editTextRepaymentAmount.getText().toString());
        double percent = Double.parseDouble(binding.editTextPercentAmount.getText().toString());
        percent/=100;
        int numberOfPayments = (int) Math.ceil(loanAmount * (1 + percent) / repaymentAmount);
        binding.seekBar.setMax(numberOfPayments);


        double debt = (loanAmount * (1+ percent) - binding.seekBar.getProgress() * repaymentAmount);

        if(debt<0) binding.editTextDebt.setText("0");
        else binding.editTextDebt.setText(String.valueOf(debt));
    }
    private void saveDataToDatabase() {
        double loanAmount = Double.parseDouble(binding.editTextLoanAmount.getText().toString());
        double repaymentAmount = Double.parseDouble(binding.editTextRepaymentAmount.getText().toString());
        double percentAmount = Double.parseDouble(binding.editTextPercentAmount.getText().toString());

        SQLiteDatabase db = databaseHelper.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(DatabaseHelper.COLUMN_LOAN_AMOUNT, loanAmount);
        values.put(DatabaseHelper.COLUMN_REPAYMENT_AMOUNT, repaymentAmount);
        values.put(DatabaseHelper.COLUMN_PERCENT_AMOUNT, percentAmount);

        lastSavedId = db.insert(DatabaseHelper.TABLE_NAME, null, values);

        db.close();
    }

    private void showPopupMenu(View view) {
        SQLiteDatabase db = databaseHelper.getReadableDatabase();
        Cursor cursor = db.query(
            DatabaseHelper.TABLE_NAME,
            new String[]{DatabaseHelper.COLUMN_ID, DatabaseHelper.COLUMN_LOAN_AMOUNT, DatabaseHelper.COLUMN_REPAYMENT_AMOUNT, DatabaseHelper.COLUMN_PERCENT_AMOUNT},
            null,
            null,
            null,
            null,
            null
        );

        PopupMenu popupMenu = new PopupMenu(requireContext(), view);
        int i = 0;
        while (cursor != null && cursor.moveToNext()) {
            String loanAmount = cursor.getString(cursor.getColumnIndexOrThrow(DatabaseHelper.COLUMN_LOAN_AMOUNT));
            String repaymentAmount = cursor.getString(cursor.getColumnIndexOrThrow(DatabaseHelper.COLUMN_REPAYMENT_AMOUNT));
            String percentAmount = cursor.getString(cursor.getColumnIndexOrThrow(DatabaseHelper.COLUMN_PERCENT_AMOUNT));

            String itemTitle = "Loan: " + loanAmount + ", Repayment: " + repaymentAmount + ", Percent: " + percentAmount;
            popupMenu.getMenu().add(Menu.NONE, i, Menu.NONE, itemTitle);
            i++;
        }

        popupMenu.setOnMenuItemClickListener(item -> {
            int itemId = item.getItemId();
            if (cursor != null && cursor.moveToPosition(itemId)) {
                double loanAmount = cursor.getDouble(cursor.getColumnIndexOrThrow(DatabaseHelper.COLUMN_LOAN_AMOUNT));
                double repaymentAmount = cursor.getDouble(cursor.getColumnIndexOrThrow(DatabaseHelper.COLUMN_REPAYMENT_AMOUNT));
                double percentAmount = cursor.getDouble(cursor.getColumnIndexOrThrow(DatabaseHelper.COLUMN_PERCENT_AMOUNT));

                binding.editTextLoanAmount.setText(String.valueOf(loanAmount));
                binding.editTextRepaymentAmount.setText(String.valueOf(repaymentAmount));
                binding.editTextPercentAmount.setText(String.valueOf(percentAmount));
            }
            return true;
        });


        popupMenu.setOnDismissListener(menu -> {
            cursor.close();
        });

        popupMenu.show();
    }


}