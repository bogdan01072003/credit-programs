package com.example.myapplication.ui.home;

import android.app.Application;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import com.example.myapplication.MainActivity;
import java.util.List;

public class HomeViewModel extends AndroidViewModel {
    private final MutableLiveData<Double> loanAmount;
    private final MutableLiveData<Double> repaymentAmount;
    private final MutableLiveData<Double> percent;


    public HomeViewModel(Application application) {
        super(application);
        loanAmount = new MutableLiveData<>();
        repaymentAmount = new MutableLiveData<>();
        percent = new MutableLiveData<>();

    }

    public LiveData<Double> getLoanAmount() {
        return loanAmount;
    }

    public LiveData<Double> getRepaymentAmount() {
        return repaymentAmount;
    }

    public LiveData<Double> getPercent() {
        return percent;
    }

    public void setLoanAmount(Double amount) {
        loanAmount.setValue(amount);
    }

    public void setRepaymentAmount(Double amount) {
        repaymentAmount.setValue(amount);
    }

    public void setPercent(Double history) {
        percent.setValue(history);
    }


}
