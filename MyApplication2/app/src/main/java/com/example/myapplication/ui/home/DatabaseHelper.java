package com.example.myapplication.ui.home;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class DatabaseHelper extends SQLiteOpenHelper {
  private static final String DATABASE_NAME = "LoanData.db";
  private static final int DATABASE_VERSION = 1;

  // Таблиця для збереження даних про кредит
  public static final String TABLE_NAME = "LoanData";
  public static final String COLUMN_ID = "id";
  public static final String COLUMN_LOAN_AMOUNT = "loan_amount";
  public static final String COLUMN_REPAYMENT_AMOUNT = "repayment_amount";
  public static final String COLUMN_PERCENT_AMOUNT = "percent_amount";

  private static final String CREATE_TABLE =
      "CREATE TABLE " + TABLE_NAME + " (" +
          COLUMN_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
          COLUMN_LOAN_AMOUNT + " REAL, " +
          COLUMN_REPAYMENT_AMOUNT + " REAL, " +
          COLUMN_PERCENT_AMOUNT + " REAL)";

  public DatabaseHelper(Context context) {
    super(context, DATABASE_NAME, null, DATABASE_VERSION);
  }

  @Override
  public void onCreate(SQLiteDatabase db) {
    db.execSQL(CREATE_TABLE);
  }

  @Override
  public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
    db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);
    onCreate(db);
  }
}
